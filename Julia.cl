/*
*
*	Compute and draw the Julia set
*	The particular set we will be using is f(z) = z^2 + c, mainly because I find it aesthetically appealing
*/

// output        : A buffer with sizeX*sizeY elements, storing
//                 the colors as RGB ints
// sizeX, sizeX  : The width and height of the buffer
// x0,y0,x1,y1   : The rectangle in which the mandelbrot
//                 set will be computed
// maxIterations : The maximum number of iterations
// colorMap      : A buffer with colorMapSize elements,
//                 containing the pixel colors


__kernel void computeJulia(
	__global uint *output,
    int sizeX, int sizeY,
    float x0, float y0,
    float x1, float y1,
    int maxIterations,
    __global uint *colorMap,
    int colorMapSize,
    float cr, float ci
)	
{
	unsigned int ix = get_global_id(0);
    unsigned int iy = get_global_id(1);
    
    // z = x + yi
    float x = x0 + ix * (x1-x0) / sizeX;
    float y = y0 + iy * (y1-y0) / sizeY;

    float magnitudeSquared = 0;
    int iteration = 0;
    while (iteration<maxIterations && magnitudeSquared<10000)
    {
    
    	float x2 = x*x;
    	float y2 = y*y;
    	
        float fr = x2 - y2 + cr;
        float fi = 2*x*y + ci;
        
        x = fr;
        y = fi;
        
        magnitudeSquared=fr*fr + fi*fi;
        ++iteration;
    }
    if (iteration == maxIterations)
    {
        output[iy*sizeX+ix] = 0;
    }
    else
    {
        float alpha = (float)iteration/maxIterations;
        int colorIndex = (int)(alpha * colorMapSize);
        output[iy*sizeX+ix] = colorMap[colorIndex];
	}
    
    
}